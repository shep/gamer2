# Extracts thread html files to database models in a better, asynchronic manner
import os, django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gamer.settings")
django.setup()
import re,sys
from bs4 import BeautifulSoup
from gamer2.models import User, Forum, Thread, Post
from dateutil import parser
import argparse
from multiprocessing import Pool

prs = argparse.ArgumentParser()
prs.add_argument("-f", "--files", help = 'newline delimited list of thread files to archive')
prs.add_argument("-t", "--threads", help = 'number of concurrent excecution threads to use')
prs.add_argument("-c", "--chunk", default=1000, help = 'amount of execution arguments to pass at a time')
args = prs.parse_args()

# Annonymous user
anon=User.objects.filter(id=0)
if not anon.exists():
    anon=User(id=0, nick="ילדת הסימס")
    anon.save()
else:
    anon=anon.first()

def archivePost(p, thread):
    # Post id
    postid=int(re.match('ReplyAnchor([0-9]+)', p['id']).group(1))
    post=Post(id=postid, thread=thread)

    # Creation time
    post.creation_time=parser.parse(p.parent.text.strip().replace(']','').replace('[','') + ' +0300')

    # Post index
    post.index=int(p.parent.parent.find('table','Structural AlignVerticalMiddle ToolBarPadding').find('a').text.replace('#',''))

    # User data
    userspace=p.parent.parent.find_next_sibling()
    userid=re.search('userid=([0-9]+)', userspace.find('a',href=re.compile('userid=([0-9]*)')).get('href'))

    # Post rating
    if thread.ratings:
        rate=p.parent.parent.find('table','Structural AlignVerticalMiddle ToolBarPadding').find('a').parent.find_next_sibling()
        if rate.text == "":
            rate=rate.find('div').find('div')
            post.color=int(re.search('background-color:#([0-9A-F]+);',rate['style']).group(1), 16)
            post.dsize=int(re.search('width:([0-9]+)%;',rate['style']).group(1))

    # Get user data if exists
    user=anon
    if userid:
        userid=userid.group(1)

        # Is user in DB?
        if User.objects.filter(id=userid).exists():
            user=User.objects.get(id=userid)
        else:
            user=User(id=int(userid))
            userspace=p.parent.parent.find_next_sibling()

            # Nick
            user.nick=userspace.find('div', 'boldText').text
            usertxt=userspace.find('td').decode()

            # Postcount
            user.messagecount=int(re.search("הודעות: ([0-9]+)", usertxt).group(1))

            # Location
            location=re.search("מ: (.*)</div>", usertxt)
            if location:
                user.location=location.group(1)

        # User karma
        if thread.ratings and user.dsize is None:
            rank=userspace.find('div', 'boldText').find_next_sibling()
            if rank.name == 'div' and rank.text != '':
                user.desc = rank.text
                bar=rank.find_next_sibling().find('div').find('div')
                try:
                    user.color=int(re.search('background-color:#([0-9A-F]+);',bar['style']).group(1), 16)
                    user.dsize=int(re.search('width:([0-9]+)%;',bar['style']).group(1))
                except:
                    pass
        user.save()
        post.author=user

    # Post content
    post.content=userspace.find('div', id="Reply" + str(postid)).decode().replace('<a href="http://cookiesession.com/עלינו/אלירן-פאר/">Eliran Pe\'er אלירן פאר</a>','')

    # Save post content
    post.save()

def archiveThread(filename):
    print(filename)
    # Load file
    with open(filename, 'rb') as f:
        h=BeautifulSoup(f, 'html5lib')

    # Get thread title
    title=h.find('span', 'forumNameBlank').text

    # Thread id
    archive=h.find('td','ForumChannelHeaderBar PrettyBoxHeadline').text == 'ארכיון'
    threadid=int(re.search('TopicID=([0-9]+)', filename).group(1))
    threadname=h.find('span', 'forumNameBlank').text

    # Forum id
    forumid=int(re.search('ForumID=([0-9]*)',h.find_all('a','forum1')[1]['href']).group(1))
    if not Forum.objects.filter(id=forumid).exists():
        forum=Forum(id=forumid)
        forum.save()
    else:
        forum=Forum.objects.get(id=forumid)

    # Thread
    posts=h.find_all('a', id=re.compile('ReplyAnchor[0-9]+'))
    if Thread.objects.filter(id=threadid).exists():
        thread=Thread.objects.get(id=threadid)
    else:
        thread=Thread(id=threadid, name=threadname, forum=forum, archive=archive)

        # Is the rating system on in this thread?
        thread.ratings= posts[0].parent.parent.find('table',
                'Structural AlignVerticalMiddle ToolBarPadding').find('a').parent.find_next_sibling().text != "בפורום זה, אין שימוש במערכת הדירוג"
        thread.save()

    # Thread posts

    for p in posts:
        try:
            archivePost(p, thread)
        except:
            sys.stderr.write(filename + " " + str(sys.exc_info()[0]))

# Reads file list from input file and excecutes thread saving accordingly
# Couldn't make it async, the mysql connection drops in new threads. Instead of more fucking around I'll just make it synchronous
files=[]
with open(args.files) as f:
    files=[l.rstrip() for l in f]
for t in files:
    try:
        archiveThread(t)
    except:
        sys.stderr.write(t + " " + str(sys.exc_info()[0]))
