# Attempts to sanitize gamer htmls and redirects from gamer to localhost
import re,sys
from bs4 import BeautifulSoup
from htmlmin import minify

# Load file
h=BeautifulSoup(open(sys.argv[1], 'rb'), 'html.parser')

# Redirects gamer domains to localhost
for e in h.find_all(href=re.compile('http:\/\/.*\.gamer\..*\.co\.il\/')):
    e['href']='../../' + re.match('http:\/\/.*\.gamer\..*\.co\.il\/(.+)', e['href']).group(1)
for e in h.find_all(src=re.compile('http:\/\/.*\.gamer\..*\.co\.il\/')):
    e['src']='../../' + re.match('http:\/\/.*\.gamer\..*\.co\.il\/(.+)', e['src']).group(1)

# Remove scripts
for s in h.find_all('script'):
    s.extract()

'''
# Remove top banner
b=h.find('table', 'Structural Header')
if b:
    b.extract()

# Remove bottom frame
f=h.find('iframe', id='ifrmNana')
if f:
    f.extract()

# Remove bottom add
a=h.find('a', href='http://www.mediagame.co.il/?view=store&p_cat=44384')
if a:
    a.parent.extract()
'''
# Save file
with open('done.html','w') as f:
    f.write(h.prettify())
