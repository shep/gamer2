import re, sys
from bs4 import BeautifulSoup
from urllib.parse import urlparse

# Open file
with open(sys.argv[1], 'rb') as f:
    h=BeautifulSoup(f, 'html.parser')

h.find_all('div')[-1].extract()

for e in h.find_all():
    if e.get('class'):
        if ("PrettyBoxHeadline" in e["class"]) or ("PrettyBoxTop" in e["class"]):
            e['style'] = "background-image: url({% static \"gamer2/skins/0/images/g3style/mainbox-top.png\" %});"

    if e.name == 'iframe':
        e.extract()

    if e.name == 'script':
        e.extract()

    elif e.get('href'):
        try:
            e['href']= "{% static \"" + re.search('(?:\.\.\/)*(.+)', urlparse(e['href']).path).group(1) + "\" %}"
        except:
            print(e)
    if e.get('src'):
        if e.name=='img':
            e['src']= "{% static \"" + re.search('(?:\.\.\/)*(.+)', urlparse(e['src']).path).group(1) + "\" %}"

with open('redirected.html', 'w') as f:
    f.write(h.prettify())
