from django.db import models

class User(models.Model):
    id=models.BigIntegerField(primary_key=True)
    location=models.CharField(max_length=150, null=True)
    nick=models.CharField(null=True, max_length=80)
    desc=models.CharField(null=True, max_length=80)
    color=models.PositiveIntegerField(null=True)
    dsize=models.PositiveSmallIntegerField(null=True)
    messagecount=models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return str(self.id) + ':' + self.nick

class ForumSynonym(models.Model):
    forum=models.ForeignKey('Forum', on_delete=models.CASCADE)
    name=models.CharField(max_length=200)
    since=models.DateField()
    until=models.DateField()

class Forum(models.Model):
    id=models.PositiveIntegerField(primary_key=True)
    name=models.CharField(max_length=150, null=True)

    def __str__(self):
        if self.name:
            return str(self.id) + ':' + self.name
        else:
            return str(self.id)

class Thread(models.Model):
    id=models.BigIntegerField(primary_key=True)
    forum=models.ForeignKey('Forum', models.CASCADE, null=True)
    name=models.CharField(max_length=255, null=True)
    archive=models.BooleanField()
    ratings=models.BooleanField()

    def __str__(self):
        if self.name:
            return str(self.id) + ':' + self.name
        else:
            return str(self.id)

class Post(models.Model):
    id=models.BigIntegerField(primary_key=True)
    author=models.ForeignKey('User', models.CASCADE, null=True)
    thread=models.ForeignKey('Thread', models.CASCADE, null=True)
    index=models.PositiveSmallIntegerField(null=True)
    creation_time=models.DateTimeField(null=True)
    content=models.TextField(null=True)
    color=models.PositiveIntegerField(null=True)
    dsize=models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return str(self.index) + '-' + str(self.id) + ':' + str(self.author) + ' on ' + str(self.creation_time)

    class Meta:
        indexes = [
                models.Index(fields=["thread", "index"]),
                models.Index(fields=["author"])
                ]
