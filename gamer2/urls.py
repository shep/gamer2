from django.urls import path
from . import views

urlpatterns = [
        path('thread/<int:thread_id>', views.thread, name='thread'),
        path('forum/<int:forum_id>', views.forum, name='forum'),
        path('user/<int:userid>', views.UserPage, name='user'),
        path('', views.forumlist, name='forumlist')
        ]
