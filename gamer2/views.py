from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse
from gamer2.models import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import math

def thread(request, thread_id, posts_per_page=30):
    thrd=Thread.objects.filter(id=thread_id)
    if not thrd.exists():
        return HttpResponse("Thread " +str(thread_id) + " not found!")
    else:
        thrd=thrd.first()
    posts=Post.objects.filter(thread=thrd).order_by('index')

    # Converts karma color to hex if ratings are enabled
    if thrd.ratings:
        for p in posts:
            if p.color:
                p.color= "{:x}".format(p.color)
            if p.author.color:
                p.author.color= "{:x}".format(p.author.color)

    # Paginate thread
    page=request.GET.get('page', 1)
    paginator=Paginator(posts, posts_per_page)

    # Flip through pages
    try:
        pageposts= paginator.page(page)
    except PageNotAnInteger:
        pageposts = paginator.page(1)
    except EmptyPage:
        pageposts = paginator.page(paginator.num_pages)

    return render(request, 'thread.html', {'posts':pageposts,
                        'pages':math.ceil(posts.count()/posts_per_page),
                        'postcount':posts.count(), 't':thrd })

def UserPage(request, userid, posts_per_page=50):
    user=User.objects.filter(id=userid)
    if not user.exists():
        return HttpResponse("User not found")
    else:
        user=user.first()
    template=loader.get_template('user.html')

    posts=Post.objects.filter(author=user).order_by('id')

    # Paginate user page
    page=request.GET.get("page", 1)
    paginator=Paginator(posts, posts_per_page)

    # Flip through pages
    try:
        pageposts=paginator.page(page)
    except PageNotAnInteger:
        pageposts=paginator(1)
    except EmptyPage:
        pageposts = paginator.page(paginator.num_pages)

    return render(request, "user.html", {'posts':pageposts,
                        'pages':math.ceil(posts.count()/posts_per_page),
                        'postcount':posts.count()})

def forumlist(request):
    fnon=Forum.objects.exclude(name__isnull=False)
    forums=Forum.objects.exclude(name__isnull=True)
    template=loader.get_template('forumlist.html')

    return HttpResponse(template.render({'forums':forums, 'unnamed':fnon}), request)

def forum(request, forum_id):
    forum=Forum.objects.get(id=int(forum_id))
    all_threads=Thread.objects.filter(forum=forum).order_by('-id')
    page = request.GET.get('page', 1)
    paginator = Paginator(all_threads, 50)
    try:
        threads = paginator.page(page)
    except PageNotAnInteger:
        threads = paginator.page(1)
    except EmptyPage:
        threads = paginator.page(paginator.num_pages)
    return render(request, 'forum.html', {'threads': threads, 'forum':forum,
        'threadcount':all_threads.count() })
