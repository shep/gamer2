# Clears all data from database
import os, django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gamer.settings")
django.setup()
from gamer2.models import *

Post.objects.all().delete()
Thread.objects.all().delete()
Forum.objects.all().delete()
User.objects.all().delete()
