# Gamer2

This is a django server meant to serve an archived version of the now deceased
forums.gamer.co.il gaming forum.
The scripts used to extract data from archived forum pages to the database are included.

## Initiation
The database connection settings are kept in credentials.json, see credentials.template.json for template.
[The forum archive is hosted here](https://archive.org/details/forums.gamer.co.il), it can be downloaded using the script get_gamer2_archive.sh and extracted to the database by running gamer2db.py on each HTML file.

## Statement
I am not affiliated with the gamer administration or owners.
The project retains this data strictly for archival purposes.
